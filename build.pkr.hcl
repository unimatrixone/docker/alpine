variable "docker_repository" { default = null }


build {

  sources = [
    "source.docker.alpine3-12",
    "source.docker.latest",
  ]

  provisioner "shell" {
    inline = [
      "apk -U upgrade",
      "addgroup -g ${local.runtime_gid} ${local.runtime_grp}",
      "adduser --home ${local.runtime_home} -u ${local.runtime_uid} --ingroup ${local.runtime_grp} --shell /bin/ash ${local.runtime_usr} -D",
      "apk -v cache clean || true",
      "cat /etc/apk/repositories",
      "apk info -v | sort",
    ]
  }

  post-processors {
    post-processor "docker-tag" {
      repository  = coalesce(var.docker_repository, local.docker_repository)
      tags        = ["3.12"]
      only        = ["docker.alpine3-12"]
    }

    post-processor "docker-tag" {
      repository  = coalesce(var.docker_repository, local.docker_repository)
      tags        = ["latest"]
      only        = ["docker.latest"]
    }

    post-processor "docker-push" {}
  }
}

