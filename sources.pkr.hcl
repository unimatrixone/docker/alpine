

source "docker" "latest" {
  image   = "alpine:latest"
  commit  = true
  changes = concat(local.docker_env, [
    "LABEL maintainer=${local.maintainer}",
    "LABEL version=latest",
  ])
}


source "docker" "alpine3-12" {
  image   = "alpine:3.12"
  commit  = true
  changes = concat(local.docker_env, [
    "LABEL maintainer=${local.maintainer}",
    "LABEL version=latest",
  ])
}
